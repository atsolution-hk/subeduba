//
//  main.m
//  Subeduba
//
//  Created by Mike Li on 2/1/2019.
//  Copyright © 2019 Mike Li. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
