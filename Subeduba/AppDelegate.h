//
//  AppDelegate.h
//  Subeduba
//
//  Created by Mike Li on 2/1/2019.
//  Copyright © 2019 Mike Li. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

